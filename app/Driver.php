<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Driver extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $table = 'nowcan_driver';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_register_status',
        'name',
        'username',
        'email',
        'password',
        'gender',
        'address',
        'phone_number',
        'identity_photo',
        'token_firebase',
        'token',
        'birthday',
        'latitude_now',
        'langitude_now',
        'bio',
        'profile_pic',
        'nik',
        'zip_postal',
        'driver_type_id',
        'license_plat',
        'vehicle_brand',
        'vehicle_type',
        'vehicle_color',
        'referral_code',
        'stnk',
        'sim',
        'kk',
        'status',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'username',
        'password',
        'token_firebase',
        'token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
