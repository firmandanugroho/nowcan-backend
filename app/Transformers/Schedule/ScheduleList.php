<?php
namespace App\Transformers\Schedule;

use App\Course;
use App\TimeSlot;
use App\Schedule;
use App\Teacher;
use App\StudentClass;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class ScheduleList extends TransformerAbstract
{
    public function transform ($schedules)
    {
      return $this->transformSchedules ($schedules);
    }

    private function transformSchedules (Collection $schedules) {
      $transformed = array();
      foreach ($schedules as $schedule) {
        $schedule = $this->transformSchedule ($schedule);
        array_push ($transformed, $schedule);
      }
      return $transformed;
    }

    private function transformSchedule (Schedule $schedule) {
      return [
        'id'             => $schedule->id,
        'hari'           => $schedule->hari,
        'mata_pelajaran' => $this->getCourse ($schedule->id_mata_pelajaran),
        'jam_pelajaran'  => $this->getTimeSlot ($schedule->id_jam_pelajaran),
        'kelas'          => $this->getClass ($schedule->id_kelas),
        'pengajar'       => $this->getTeacher ($schedule->id_pengajar),
      ];
    }

    private function getCourse ($courseId) {
      return Course::select('nama')->where('id', $courseId)->first();
    }

    private function getTimeSlot ($timeSlotId) {
      return
        TimeSlot::select('waktu_mulai', 'waktu_selesai')
                ->where('id', $timeSlotId)
                ->first();
    }

    private function getClass ($classId) {
      return
        StudentClass::select('nama_kelas')
                    ->where('id', $classId)
                    ->first();
    }

    private function getTeacher ($teacherId) {
      return Teacher::select('nama')->where('id', $teacherId)->first();
    }
}
