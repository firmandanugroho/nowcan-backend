<?php
namespace App\Transformers\StudentClass;

use App\StudentClass;
use App\Student;
use App\Teacher;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class ClassList extends TransformerAbstract
{
    public function transform ($classes)
    {
      return $this->transformClasses ($classes);
    }

    private function transformClasses (Collection $classes) {
      $transformed = array();
      foreach ($classes as $class) {
        $class = $this->transformClass ($class);
        array_push($transformed, $class);
      }
      return $transformed;
    }

    private function transformClass (StudentClass $class) {
      return [
        'id'         => $class->id,
        'nama_kelas' => $class->nama_kelas,
        'wali_kelas' => $this->getHomeroomTeacher($class->id),
        'siswa'      => $this->getStudents($class->id),
      ];
    }

    private function getStudents ($classId) {
      return Student::where('id_kelas', $classId)->get();
    }

    private function getHomeroomTeacher ($teacherId) {
      return Teacher::where('id', $teacherId)->first();
    }
}
