<?php
namespace App\Transformers\ReportCard;

use App\Course;
use App\Student;
use App\StudentClass;
use App\FinalGrade;
use App\ReportCard;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class ReportCardTransformer extends TransformerAbstract
{
    public function transform (ReportCard $reportCard)
    {
      return $this->transformReportCard ($reportCard);
    }

    private function transformReportCard (ReportCard $reportCard)
    {
      return [
        'id'           => $reportCard->id,
        'tahun_ajaran' => $reportCard->tahun_ajaran,
        'semester'     => $reportCard->semester,
        'siswa'        => $this->getStudent($reportCard->id_siswa),
        'ranking'      => $reportCard->ranking,
        'nilai'        => $this->getFinalGrades($reportCard->id),
      ];
    }

    private function getStudent ($studentId)
    {
      $student = Student::find($studentId);
      $student = [
        'nisn'  => $student->nisn,
        'nis'   => $student->nis,
        'nama'  => $student->nama,
        'kelas' => StudentClass::find($student->id_kelas)->nama_kelas,
      ];
      return $student;
    }

    private function getFinalGrades ($reportCardId)
    {
      $filter = ['id_rapor' => $reportCardId];
      $grades = FinalGrade::where($filter)->get();
      $finalGrades = [];
      foreach ($grades as $grade) {
        $grade = $this->getFinalGrade ($grade);
        array_push($finalGrades, $grade);
      }
      return $finalGrades;
    }

    private function getFinalGrade ($grade)
    {
      $grade = [
        'mata_pelajaran' => Course::find($grade->id_mata_pelajaran)->nama,
        'nilai'          => $grade->nilai_akhir,
      ];
      return $grade;
    }
}
