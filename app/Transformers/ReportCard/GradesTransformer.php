<?php
namespace App\Transformers\ReportCard;

use App\Course;
use App\Grade;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class GradesTransformer extends TransformerAbstract
{
    public function transform ($grades)
    {
      return $this->transformGrades ($grades);
    }

    private function transformGrades (Collection $grades)
    {
      $transformed = array();
      foreach ($grades as $grade) {
        $grade = $this->transformGrade ($grade);
        array_push ($transformed, $grade);
      }
      return $transformed;
    }

    private function transformGrade (Grade $grade)
    {
      return [
        'id'             => $grade->id,
        'tahun_ajaran'   => $grade->tahun_ajaran,
        'semester'       => $grade->semester,
        'mata_pelajaran' => $this->getCourse ($grade->id_mata_pelajaran),
        'judul_nilai'    => $grade->judul_nilai,
        'nilai'          => $grade->nilai,
      ];
    }

    private function getCourse ($courseId)
    {
      $course = Course::find($courseId)->nama;
      return $course;
    }
}
