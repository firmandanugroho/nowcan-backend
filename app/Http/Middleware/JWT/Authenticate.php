<?php

namespace App\Http\Middleware\JWT;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cookie;
use Tymon\JWTAuth\JWTAuth;

class Authenticate
{
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function handle($request, Closure $next, ...$roles)
    {
        if (!$this->isAuthorized($roles)) {
          return response()
            ->json([
              'code'    => 401,
              'message' => 'unauthorized'
            ], 401);
        }
        return $next($request);
    }

    private function checkToken () {
      if ($token = Cookie::get('token')) {
        $this->jwt->setToken($token);
        return $this->jwt->user();
      }
      return null;
    }

    private function isAuthorized ($roles) {
      if ($user = $this->checkToken()) {
        foreach ($roles as $role) {
          if ($user->role == $role) {
            return true;
          }
        }
      }
      return false;
    }
}
