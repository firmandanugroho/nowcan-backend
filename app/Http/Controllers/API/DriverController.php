<?php

namespace App\Http\Controllers\API;

use App\Driver;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;
use Tymon\JWTAuth\JWTAuth;

class DriverController extends Controller
{
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
      $this->jwt = $jwt;
    }

    public function register (Request $request)
    {
      try {
        $register_status = ['id_register_status' => '1'];
        $password = ['password' => Hash::make($request->input('password'))];
        $kk = $this->uploadDocumentImage ('kk', $request->file('foto_kk'));
        $sim = $this->uploadDocumentImage ('sim', $request->file('foto_sim'));
        $stnk = $this->uploadDocumentImage ('stnk', $request->file('foto_stnk'));
        $pp = $this->uploadDocumentImage ('profile_pic', $request->file('profile_pic'));
        $ip = $this->uploadDocumentImage ('identity_photo', $request->file('identity_photo'));
        $driver = array_merge(
          $request->all(), $password, $register_status, $kk, $stnk, $sim, $ip, $pp
        );
        Driver::create($driver);
      }
      catch (\Exception $e) {
        return response()
          ->json([
            'code'  => 500,
            'error' => $e->getMessage(),
          ], 500);
      }
      return response()
        ->json([
          'code'    => 200,
          'message' => 'driver registered',
        ], 200);
    }

    public function login (Request $request)
    {
      try {
        $credentials = $request->only(['username', 'password']);
        if (!$token = $this->jwt->attempt($credentials)) {
          return response()
            ->json([
              'code' => 401,
              'error' => 'unauthorized',
            ], 401);
          }
        $driver = $this->jwt->user();
      }
      catch (\Exception $e) {
        return response()
          ->json([
            'code'  => 500,
            'error' => $e->getMessage()
        ], 500);
      }
      return response()
        ->json([
          'code'    => 200,
          'message' => 'driver logged in',
          'token'   => $token,
          'driver'  => $driver,
        ], 200);
    }

    private function uploadDocumentImage ($doc, $photo) {
      if($photo!=null){
        $filename = $doc.time().'.'.$photo->getClientOriginalExtension();
        $destination = storage_path('app'.'/'.$doc);
        $file = $photo->move($destination, $filename);
        return [$doc => 'storage/app'.'/'.$doc.'/'.$filename];
      }else{
        return [$doc => '-'];;
      }
    }
}
